# xmlstreamer

Package xmlstreamer writes XML streams.


## Overview

 - Dynamically constructs XML documents.
 - Does not require use of structs to encode data.
 - Does not provide means to encode data to XML.
 - Allows injecting raw data.


## Basic usage

Document structure is constructed by opening and closing elements manually. New
elements are created by providing its name. Possible namespace must be given as
part of element name. Closing an element pops an element from top of the stack.

Element attributes can be added dynamically as long as an element is
incomplete. When a new element is created, it remains incomplete until it is
explicitly opened, a child or a sibling element is created, or cdata is
written.


```go
package main

import (
        "os"

        "gitlab.com/tsaviran/xmlstream"
)

func main() {
        // to io
        w := xmlstream.NewWriter(os.Stdout)

        // to string
        // buf := bytes.NewBuffer("")
        // w := xmlstream.NewWriter(buf)

        // to bufio
        // note: does not autoflush by default
        // buf := bufio.NewWriter(os.Stdout)
        // w := xmlstream.NewWriter(buf)
        // w.FlushCB = func() error { return buf.Flush() }

        w.StartElement("foo")
        w.StartElement("lemon")
        w.AddAttribute("sour", "true")
        w.WriteCData("it's yellow")
        w.EndElement()
        w.ElementCData("orange", "foo")
        w.Finalise()
}
```

This will produce the following XML document (indented for convenience):

```xml
<?xml version="1.0" encoding="UTF-8"?>
<foo>
  <lemon sour="true">it's yellow</lemon>
  <orange>foo</orange>
</foo>
```


### Buffering

It's possible to use buffered writers. However notice that you'll have the
ultimate responsibility to flush the buffers. You can instruct Writer to
perform a flush on Finalise. While it may be counter-productive, it is also
possible to request xmlstream to flush the io whenever its internal state
allows it.

```go
b := bufio.NewWriter(os.Stdout)
w := xmlstream.NewWriter(b)
w.FlushCB = func() error { return b.Flush() }
w.AutoFlush = true
```


## Examples

Tests are expected to cover all aspects to xmlstream.

`xmlstreamer` is a streaming XML writer.
