package xmlstream_test

import (
	"bytes"
	"encoding/xml"
	"testing"

	"gitlab.com/tsaviran/xmlstream"
)

func TestRawTop(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.WriteRaw("<a/>")
	w.Finalise()

	out := buf.String()[len(xml.Header):]
	exp := `<a/>`
	if out != exp {
		t.Errorf("didn't write raw at top - expected: %q, got %q",
			exp, out)
	}
}

func TestRawBroken(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.OpenElement()
	w.WriteRaw("<foo")
	w.EndElement()
	w.Finalise()

	out := buf.String()[len(xml.Header):]
	exp := `<a><foo</a>`
	if out != exp {
		t.Errorf("didn't write raw as is - expected: %q, got %q",
			exp, out)
	}
}

func TestFinaliseElementOpen(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.OpenElement()
	w.WriteRaw("<b")
	w.EndElement()

	out := buf.String()[len(xml.Header):]
	exp := `<a><b</a>`
	if out != exp {
		t.Errorf("didn't write raw as is - expected: %q, got %q",
			exp, out)
	}
}

func TestRawFinalise(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.WriteRaw("<foo")
	w.Finalise()

	out := buf.String()[len(xml.Header):]
	exp := `<a<foo/>`
	if out != exp {
		t.Errorf("didn't handle finishing raw - expected: %q, got %q",
			exp, out)
	}
}
