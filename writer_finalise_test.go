package xmlstream_test

import (
	"bytes"
	"encoding/xml"
	"testing"

	"gitlab.com/tsaviran/xmlstream"
)

func TestFinaliseOne(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.Finalise()

	out := buf.String()[len(xml.Header):]
	exp := `<a/>`
	if out != exp {
		t.Errorf("didn't finalise self-closing - expected: %q, got %q",
			exp, out)
	}
}

func TestFinaliseNestedAttrib(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.AddAttribute("b", "1")
	w.StartElement("c")
	w.AddAttribute("d", "4")
	w.Finalise()

	out := buf.String()[len(xml.Header):]
	exp := `<a b="1"><c d="4"/></a>`
	if out != exp {
		t.Errorf("didn't finalise nested - expected: %q, got %q",
			exp, out)
	}
}

func TestFinaliseCData(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.WriteCData("x")
	w.Finalise()

	out := buf.String()[len(xml.Header):]
	exp := `<a>x</a>`
	if out != exp {
		t.Errorf("didn't finalise cdata - expected: %q, got %q",
			exp, out)
	}
}
