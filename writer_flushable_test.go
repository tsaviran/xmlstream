package xmlstream_test

import (
	"bufio"
	"bytes"
	"encoding/xml"
	"testing"

	"gitlab.com/tsaviran/xmlstream"
)

func TestBufio(t *testing.T) {
	buf := bytes.NewBufferString("")
	bio := bufio.NewWriter(buf)
	w := xmlstream.NewWriter(bio)
	w.StartElement("a")
	w.Finalise()
	bio.Flush()

	out := buf.String()[len(xml.Header):]
	exp := `<a/>`
	if out != exp {
		t.Errorf("expected: %q, got %q",
			exp, out)
	}
}

func TestBufioAutoFlush(t *testing.T) {
	buf := bytes.NewBufferString("")
	bio := bufio.NewWriter(buf)
	w := xmlstream.NewWriter(bio)
	w.FlushCB = func() error { return bio.Flush() }
	w.AutoFlush = true
	w.StartElement("a")
	w.Finalise()

	out := buf.String()[len(xml.Header):]
	exp := `<a/>`
	if out != exp {
		t.Errorf("expected: %q, got %q",
			exp, out)
	}
}
