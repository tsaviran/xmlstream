package xmlstream_test

import (
	"bytes"
	"encoding/xml"
	"testing"

	"gitlab.com/tsaviran/xmlstream"
)

func TestCDataTop(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	err := w.WriteCData("x")
	if err == nil {
		t.Errorf("allowed cdata at root")
	}
}

func TestCDataAttribElement(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.WriteCData("x")
	w.EndElement()

	out := buf.String()[len(xml.Header):]
	exp := `<a>x</a>`
	if out != exp {
		t.Errorf("cdata - expected: %q, got %q",
			exp, out)
	}
}

func TestCDataAttribValElement(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.AddAttribute("b", "1")
	w.WriteCData("x")
	w.EndElement()

	out := buf.String()[len(xml.Header):]
	exp := `<a b="1">x</a>`
	if out != exp {
		t.Errorf("cdata - expected: %q, got %q",
			exp, out)
	}
}

func TestCDataOpenElement(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.WriteCData("x")
	w.WriteCData("y")
	w.EndElement()

	out := buf.String()[len(xml.Header):]
	exp := `<a>xy</a>`
	if out != exp {
		t.Errorf("cdata - expected: %q, got %q",
			exp, out)
	}
}
