package xmlstream_test

import (
	"bytes"
	"encoding/xml"
	"testing"

	"gitlab.com/tsaviran/xmlstream"
)

func TestDocument(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.Finalise()
	result := buf.String()
	if result != xml.Header {
		t.Errorf("didn't write header")
	}
}
