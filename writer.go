// Package xmlstream provides functions to manually construct XML in a manner
// suitable for streamed output.
package xmlstream

import (
	"encoding/xml"
	"fmt"
	"io"
)

type elemState int

const (
	na         = iota // no element in stack
	incomplete        // <foo  - element incomplete, open for attributes
	open              // <foo> - element declared
)

// Writer is xmlstream writer.
type Writer struct {
	io        io.Writer
	elemState elemState
	hasRoot   bool
	stack     []string
	AutoFlush bool
	FlushCB   func() error
}

// Flush flushes bufio.Writer associated with Writer.
func (w *Writer) Flush() error {
	return w.FlushCB()
}

func (w *Writer) autoFlush() {
	if w.AutoFlush {
		w.Flush()
	}
}

func (w *Writer) startDocument() {
	fmt.Fprintf(w.io, xml.Header)
}

// Finalise closes all incomplete and open elements and flushes output.
func (w *Writer) Finalise() {
	for range w.stack {
		w.EndElement()
	}
	w.stack = []string{}
	w.Flush()
}

// OpenElement finalises declaring an incomplete element.
// Users should only need to call this function prior to writing raw data.
func (w *Writer) OpenElement() {
	if w.elemState != incomplete {
		return
	}
	fmt.Fprint(w.io, ">")
	w.autoFlush()
	w.elemState = open
}

// StartElement creates a new incomplete element.
// Any incomplete element is opened automatically.
// It returns any error encountered.
func (w *Writer) StartElement(name string) error {
	if len(w.stack) == 0 {
		if w.hasRoot {
			return fmt.Errorf("root element already written")
		}
		w.hasRoot = true
	}
	if w.elemState == incomplete {
		fmt.Fprintf(w.io, ">")
	}
	fmt.Fprintf(w.io, "<%s", name)
	w.autoFlush()
	w.elemState = incomplete
	w.stack = append(w.stack, name)
	return nil
}

// AddAttribute adds a new attribute to an incomplete element.
// It returns any error encountered.
func (w *Writer) AddAttribute(name, value string) error {
	if w.elemState != incomplete {
		return fmt.Errorf("no element open for attributes")
	}
	fmt.Fprintf(w.io, ` %s="%s"`, name, value)
	w.autoFlush()
	return nil
}

// EndElement closes any incomplete or open element.
// It returns any error encountered.
func (w *Writer) EndElement() error {
	n := len(w.stack)
	if n == 0 {
		return fmt.Errorf("no elements in stack")
	}
	if w.elemState == incomplete {
		fmt.Fprintf(w.io, "/>")
	} else {
		name := w.stack[n-1]
		fmt.Fprintf(w.io, "</%s>", name)
	}
	w.stack = w.stack[0:(n - 1)]
	w.elemState = open
	w.Flush()
	return nil
}

// ElementCData opens and element, writes CData, and closes the element.
// Any incomplete element is opened automatically.
// It returns any error encountered.
func (w *Writer) ElementCData(el, s string) error {
	if err := w.StartElement(el); err != nil {
		return err
	}
	if err := w.WriteCData(s); err != nil {
		return err
	}
	return w.EndElement()
}

// WriteCData writes CData.
// Any incomplete element is opened automatically.
// It returns any error encountered.
func (w *Writer) WriteCData(s string) error {
	if len(w.stack) == 0 {
		return fmt.Errorf("no open element")
	}
	w.OpenElement()
	fmt.Fprintf(w.io, s)
	w.autoFlush()
	return nil
}

// WriteComment writes a comment.
// Any incomplete element is opened automatically.
func (w *Writer) WriteComment(s string) {
	w.OpenElement()
	fmt.Fprintf(w.io, "<!-- %s -->", s)
	w.autoFlush()
}

// WriteRaw writes raw data.
func (w *Writer) WriteRaw(s string) {
	fmt.Fprint(w.io, s)
}

// NewWriter creates and returns new streaming XML writer.
func NewWriter(out io.Writer) *Writer {
	w := Writer{
		io:        out,
		stack:     []string{},
		elemState: na,
		FlushCB:   func() error { return nil },
	}
	w.startDocument()
	return &w
}
