package xmlstream_test

import (
	"bytes"
	"encoding/xml"
	"testing"

	"gitlab.com/tsaviran/xmlstream"
)

func TestElement(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.EndElement()
	w.Flush()

	out := buf.String()[len(xml.Header):]
	exp := `<a/>`
	if out != exp {
		t.Errorf("self-closing - expected: %q, got %q",
			exp, out)
	}
}

func TestElementAttrib(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.AddAttribute("b", "1")
	w.AddAttribute("c", "x")
	w.EndElement()
	w.Flush()

	out := buf.String()[len(xml.Header):]
	exp := `<a b="1" c="x"/>`
	if out != exp {
		t.Errorf("self-closing - expected: %q, got %q",
			exp, out)
	}
}

func TestElementLateAttrib(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.WriteCData("x")
	err := w.AddAttribute("b", "1")
	if err == nil {
		t.Errorf("late attribute didn't return error")
	}
}

func TestNestedElementSelf(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.StartElement("b")
	w.EndElement()
	w.EndElement()
	w.Flush()

	out := buf.String()[len(xml.Header):]
	exp := `<a><b/></a>`
	if out != exp {
		t.Errorf("nested elements - expected: %q, got %q",
			exp, out)
	}
}

func TestNestedElementAttribSelf(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.AddAttribute("b", "1")
	w.StartElement("c")
	w.AddAttribute("d", "2")
	w.EndElement()
	w.EndElement()
	w.Flush()

	out := buf.String()[len(xml.Header):]
	exp := `<a b="1"><c d="2"/></a>`
	if out != exp {
		t.Errorf("nested elements - expected: %q, got %q",
			exp, out)
	}
}

func TestSameLevelElement(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.StartElement("b")
	w.EndElement()
	w.StartElement("b")
	w.EndElement()
	w.StartElement("c")
	w.EndElement()
	w.EndElement()

	out := buf.String()[len(xml.Header):]
	exp := `<a><b/><b/><c/></a>`
	if out != exp {
		t.Errorf("same-level elements - expected: %q, got %q",
			exp, out)
	}
}

func TestDoubleRoot(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.EndElement()
	err := w.StartElement("b")
	if err == nil {
		t.Errorf("allowed multiple elements at root")
	}
}

func TestElementCDataTop(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.ElementCData("a", "b")
	w.EndElement()
	w.Flush()

	out := buf.String()[len(xml.Header):]
	exp := `<a>b</a>`
	if out != exp {
		t.Errorf("element cdata at top - expected: %q, got %q",
			exp, out)
	}
}

func TestElementCData(t *testing.T) {
	buf := bytes.NewBufferString("")
	w := xmlstream.NewWriter(buf)
	w.StartElement("a")
	w.ElementCData("b", "c")
	w.EndElement()
	w.Flush()

	out := buf.String()[len(xml.Header):]
	exp := `<a><b>c</b></a>`
	if out != exp {
		t.Errorf("element cdata - expected: %q, got %q",
			exp, out)
	}
}
